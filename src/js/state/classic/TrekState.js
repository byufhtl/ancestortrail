/**
 * Created by calvinmcm on 6/17/16.
 */


define(['../IState'],function(IState){

    function TrekState(){
        this.treasure = "...with treasure."
    }

    TrekState.prototype = new IState();

    TrekState.prototype.start = function(){
        console.log("We're starting now.", this.treasure)
    };

    return TrekState;
});
/**
 * Created by calvinmcm on 6/15/16.
 */

define(['state/StateManager', 'DataFacade', 'contexts/OregonTrailContext', 'testingCanvas'],function(StateManager, DataFacade, OregonTrailContext, canvasImage){

    var myCanvasImage;
    /**
     * The primary entrance point for the game.
     * @constructor
     */
    function Controller(){

        this.stateManager = StateManager.thing();
        this.stateManager.init(null);
        this.dataFacade = new DataFacade(OregonTrailContext);


    }

    Controller.thing = function(){
        if(!this.SINGLETON){
            this.SINGLETON = new Controller();
        }
    };

    Controller.prototype.start = function(){
        //Do stuff.
        console.log("running start");
        console.log(this.dataFacade.getContext().getResource('food'));
        myCanvasImage = new canvasImage();
        myCanvasImage.drawCanvas();
    };

    return Controller;

});
/**
 * Created by calvinmcm on 6/17/16.
 */

define(['state/StateFactory'],function(StateFactory){

    /**
     * Manages the states of gameplay
     * @constructor
     */
    function StateManager(){
        this.focusState = null;
        this.states = [];
    }

    StateManager.thing = function () {
        if(!this.SINGLETON){
            this.SINGLETON = new StateManager();
        }
        return this.SINGLETON;
    };

    StateManager.prototype.init = function(Context){
        this.states = new StateFactory(null);
        console.log(this.states[0]);
        this.states[0].start();
    };

    StateManager.prototype.switchToState = function(state, params){

    };

    StateManager.prototype.notify = function(note){

    };

    return StateManager;
});
/**
 * Created by calvinmcm on 6/17/16.
 */

define(['state/classic/TrekState'],function(ClassicTrek){

    function StateFactory(context){
        return[new ClassicTrek()];
    }

    return StateFactory;

});
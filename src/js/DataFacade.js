/**
 * Created by calvinmcm on 6/17/16.
 */

define(['contexts/IContext'],function(contextInterface){

    /**
     * The data flow controller.
     * @constructor
     */
    function DataFacade(context){
        

        var myContext = new context();
        myContext.prototype = new contextInterface();
        this.context = myContext;
    }
    
    DataFacade.prototype.getContext = function()
    {
        return this.context;
    };


    //example methods for updating the model
    DataFacade.prototype.updateResource = function(resourceType, amount)
    {
      //Use change to update the model
        
    };

    return DataFacade;

});
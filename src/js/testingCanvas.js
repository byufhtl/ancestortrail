/**
 * Created by ztnelson on 6/20/16.
 */

define(['state/StateManager', 'DataFacade', 'contexts/OregonTrailContext'],function(StateManager, DataFacade, OregonTrailContext){


function testingCanvas()
{
    this.myImage = new Image();
    this.imageLoaded  = false;
    var self = this;
    this.myImage.onload = function () {
        self.imageLoaded = true;
    };
    this.myImage.src = 'src/img/handcart_painting.jpg';
}



testingCanvas.prototype.drawCanvas = function() {
    var canvas = $("#myCanvas").get(0);
    canvas.width = $("body").width() - $('#leftBar').width() - 5;
    canvas.height = $('#leftBar').height();
    var ctx = canvas.getContext("2d");
    if (this.imageLoaded)
    {
        ctx.drawImage(this.myImage, 0, 0, this.myImage.width, this.myImage.height, 0, 0, canvas.width, canvas.height);
    }

    //requestAnimationFrame(this.drawCanvas);
};

return testingCanvas;
});
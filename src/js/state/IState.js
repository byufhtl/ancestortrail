/**
 * Created by calvinmcm on 6/17/16.
 */

define([],function(){

    function IState(){

    }

    IState.prototype.start = function(params){
        console.log("IState.prototype.start() was not overridden.");
    };

    IState.prototype.stop = function(params){
        console.log("IState.prototype.stop() was not overridden.");
    };

    IState.prototype.update = function(params){
        console.log("IState.prototype.update() was not overridden.");
    };

    return IState;

});
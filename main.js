/**
 * Created by calvinmcm on 6/16/16.
 */

require.config({
    baseUrl: 'src/js',
    paths: {
        controller: 'Controller',
        familysearch: 'https://cdn.jsdelivr.net/familysearch-javascript-sdk/2.1.0/familysearch-javascript-sdk.min',
        jquery: 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min',
        bootstrap: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min'
    },
    shim: {
        controller: {
            deps: ['jquery']
        },
        familysearch:{
            deps: ['jquery']
        },
        bootstrap: {
            deps: ['jquery']
        }
    }
});

require(['jquery', 'controller', 'familysearch', 'bootstrap'],function($, App, FamilySearch, Bootstrap){
    console.log(App);
    var app = new App();
    app.start();
});


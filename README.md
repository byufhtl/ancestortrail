# README #

**Ancestor Trail**

### SUMMARY ###

Ancestor Trail will be a free, open-source version of the game Oregon Trail with a Family History spin using data from FamilySearch.

Version: -1.0

Language: Javascript, HTML, CSS

### SETUP ###

Open your favorite web page development environment and go.

### CONTRIBUTION ###

Pull before pushing.
Don't mess up.

### REFERENCE ###

Leads: Calvin and Zach
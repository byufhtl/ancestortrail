/**
 * Created by calvinmcm on 6/20/16.
 */

define(['jquery'],function($){

    function IViewportManager(){

    }

    IViewportManager.prototype.start = function(){
        console.log("IViewportManager.start() not implemented in child.");
    };

    IViewportManager.prototype.close = function(){
        console.log("IViewportManager.close() not implemented in child.");
    };

    IViewportManager.prototype.wipeViewPort = function(){
        var canvas = $('#viewport');
        var context = canvas.getContext();

        context.save(); // Store old context.
        context.setTransform(1, 0, 0, 1, 0, 0); //
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.restore();
    }
});